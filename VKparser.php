<?php

class VKparser
{
    //заполнить своими данными
    private $group = 'mydotaru';
    private $access_token = '';

    //id беседы в которую отправляем сообщение
    private $chat_id = '138';

    //нужны для отправки сообщения конкретному пользователю
    private $user_id = '';
    private $domain = '';

    private $url = 'https://api.vk.com/method/';
    private $owner_id;
    private $dateToday;
    private $dateYesterday;
    private $nameGroup;
    private $posts = [];
    private $comments = 0;

    //сам парсер который отвечает за все
    public function parser()
    {
        $this->dateToday = time();
        $this->dateYesterday = $this->dateToday - (24 * 60 * 60);

        $people = $this->getGroupId();
        $posts = $this->getPosts();

        if (!$posts) {
            return $this->sendMessage('Не получилось просчитать ER, попробуйте позже');
        }
        $countLikes = 0;
        $countReposts = 0;
        $countComments = 0;

        foreach ($this->posts as $post)
        {
            $countLikes += $this->getList($post);
            $countReposts += $this->getList($post, 'copies');
            $countComments += $this->getComments($post, '0');
        }

        $er = (($countLikes + $countReposts + $countComments) / $people) * 100;
        $er = round($er, 2);

        $er_yesterday = file_get_contents('er.txt');

        setlocale(LC_ALL, "ru_RU.UTF-8");
        $today = $this->strftime_rus("%e %B2 %H:%M", $this->dateToday);
        $yesterday = $this->strftime_rus("%e %B2 %H:%M", $this->dateYesterday);

        $text_message = "С " . $yesterday . " по " . $today . " — ER группы \"$this->nameGroup\" составил " . $er . "%, ";

        if ($er_yesterday > $er)
        {
            $difference = round($er_yesterday - $er, 2);
            $text_message .= "за последний час ER уменьшился на " . $difference . "%" . "\r\n";
        }
        else
        {
            $difference = round($er - $er_yesterday, 2);
            $text_message .= "за последний час ER увеличился на " . $difference . "%" . "\r\n";
        }

        $this->sendMessage($text_message);

        file_put_contents('message.txt', $text_message, FILE_APPEND);
        file_put_contents('er.txt', $er);

        echo $text_message;
    }

    //отправка сообщения
    protected function sendMessage($message)
    {
        $params = [
            'user_id' => $this->user_id,
            'domain' => $this->domain,
            'chat_id' => $this->chat_id,
            'message' => $message,
            'access_token' => $this->access_token,
        ];

        $url = $this->url . 'messages.send?' . http_build_query($params);

        // если без прокси не работает, то вызывать не напрямую file_get_contents а через прокси $this->proxy($url);
        $res = file_get_contents($url);

        return true;
    }

    //Возвращает информацию о заданном сообществе или о нескольких сообществах.
    protected function getGroupId()
    {
        $fields = ['id', 'name', 'members_count'];

        $params = [
            'group_id' => $this->group,
            'fields' => implode(',', $fields),
            'access_token' => $this->access_token,
        ];

        $url = $this->url . 'groups.getById?' . http_build_query($params);
        // если без прокси не работает, то вызывать не напрямую file_get_contents а через прокси
        $res = file_get_contents($url);

        $request = json_decode($res, true);
        $data = $request['response'];
        $this->owner_id = $data[0]['gid'];
        $this->nameGroup = $data[0]['name'];

        return $data[0]['members_count'];
    }

    //Cписок id posts за сутки
    protected function getPosts($offset = 0)
    {
        $params = [
            'owner_id' => '-' . $this->owner_id,
            'domain' => $this->group,
            'offset' => $offset,
            'count' => 100, //max
            'extended' => ' 1',
            'access_token' => $this->access_token,
        ];

        $url = $this->url . 'wall.get?' . http_build_query($params);
        // если без прокси не работает, то вызывать не напрямую file_get_contents а через прокси $this->proxy($url);
        $res = file_get_contents($url);

        //получаем последние 100 постов
        $this->getIdPosts($res);

        return true;
    }

    protected function getIdPosts($request)
    {
        $res = json_decode($request, true);
        $data = $res['response']['wall'];
        $posts = $this->arrayPostsId($data);
        $this->posts = array_merge($this->posts, $posts);

        if (count($posts) == 100)
        {
            $this->getPosts(100);
        }

        return true;
    }

    protected function arrayPostsId($data)
    {
        $posts = [];
        $i = 0;
        foreach ($data as $key => $value)
        {
            if (is_array($value) && ($value['date'] <= $this->dateToday && $value['date'] > $this->dateYesterday))
            {
                $posts[$i] = $value['id'];
                $i++;
            }
        }

        return $posts;
    }

    //получаем колличество лайков/репостов по конкретному посту
    protected function getList($post_id, $filter = 'likes')
    {
        $params = [
            'type' => 'post',
            'owner_id' => '-' . $this->owner_id,
            'item_id' => $post_id,
            'filter' => $filter,
            'offset' => 0,
            'count' => 1000, //max
            'access_token' => $this->access_token,
        ];

        $url = $this->url . 'likes.getList?' . http_build_query($params);
        // если без прокси не работает, то вызывать не напрямую file_get_contents а через прокси $this->proxy($url);
        $res = file_get_contents($url);
        $data = json_decode($res, true);

        if (is_array($data) && array_key_exists('response', $data) && array_key_exists('users', $data['response']))
        {
            return count($data['response']['users']);
        }
    }

    //получаем колличество комментов
    protected function getComments($post_id, $offset = 0)
    {
        $params = [
            'owner_id' => '-' . $this->owner_id,
            'post_id' => $post_id,
            'offset' => $offset,
            'count' => 100, //max
            'preview_length' => 10,
            'access_token' => $this->access_token,
        ];

        $url = $this->url . 'wall.getComments?' . http_build_query($params);

        // если без прокси не работает, то вызывать не напрямую file_get_contents($url) а через прокси $this->proxy($url);
        $res = file_get_contents($url);

        $this->getCountComments($post_id, $res);
        $countComments = $this->comments;
        $this->comments = 0;

        return $countComments;
    }

    protected function getCountComments($post_id, $request)
    {
        $res = json_decode($request, true);
        $data = $res['response'];

        $comments = count($data);
        $this->comments += $comments;

        if (count($comments) == 100)
        {
            $this->getComments($post_id, 100);
        }

        return $this->comments;
    }

    //нужен для украины))
    protected function proxy($url)
    {
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" . "Cookie: foo=bar\r\n",
                'proxy' => 'tcp://188.225.75.239:22222',
            ),
        );

        $context = stream_context_create($opts);
        $res = file_get_contents($url, false, $context);

        return $res;
    }

    //преобразуем месяца в родительный падеж
    protected function strftime_rus($format, $date = false)
    {
        if (!$date)
        {
            $timestamp = time();
        }
        else if (!is_numeric($date))
        {
            $timestamp = strtotime($date);
        }
        else
        {
            $timestamp = $date;
        }

        if (strpos($format, '%B2') === false)
        {
            return strftime($format, $timestamp);
        }

        $month_number = date('n', $timestamp);

        switch ($month_number) {
            case 1: $rus = 'января'; break;
            case 2: $rus = 'февраля'; break;
            case 3: $rus = 'марта'; break;
            case 4: $rus = 'апреля'; break;
            case 5: $rus = 'мая'; break;
            case 6: $rus = 'июня'; break;
            case 7: $rus = 'июля'; break;
            case 8: $rus = 'августа'; break;
            case 9: $rus = 'сентября'; break;
            case 10:$rus = 'октября'; break;
            case 11:$rus = 'ноября'; break;
            case 12:$rus = 'декабря'; break;
        }

        $rusformat = str_replace('%B2', $rus, $format);

        return strftime($rusformat, $timestamp);
    }
}

$vk = new VKparser();
$vk->parser();